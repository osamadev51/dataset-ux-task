var minSidebarWidth = 200;
var maxSidebarWidth = 700;
var Scrollbar = window.Scrollbar;
var elem = document.querySelector("#scrollbar-custom");
var scrollbarOptions = {
  alwaysShowTracks: true,
  renderByPixels: true
};

var validationObj = [
  {
    //order_date
    regex: new RegExp("([12]\\d{3}/(0[1-9]|1[0-2])/(0[1-9]|[12]\\d|3[01]))$")
  },
  {
    //pages_visited
    regex: new RegExp("^[0-9]*$")
  },
  {
    //order_id
    regex: new RegExp("HTS-[0-9]{4}$")
  },
  {
    //customer_id
    regex: new RegExp("^[0-9]{6}$")
  },
  {
    //tshirt_category
    regex: new RegExp("[a-zA-Z][a-zA-Z ]$")
  },
  {
    //tshirt_price
    regex: new RegExp("([0-9]*[.])?[0-9]+$")
  },
  {
    //tshirt_quantity
    regex: new RegExp("^[0-9]*$")
  }
];

$(document).ready(function() {
  Scrollbar.init(elem, scrollbarOptions);
  datatableInit();
  if (!isMobileOrTablet()) {
    sideBarResizer();
  } else {
    sideBarToggle();
  }
});

function sideBarResizer() {
  $(".sidebar-resizer").on("mousedown", function(e) {
    e.preventDefault();
    $(document).mousemove(function(e) {
      e.preventDefault();
      var x = e.pageX - $(".sidebar").offset().left;
      if (
        x > minSidebarWidth &&
        x < maxSidebarWidth &&
        e.pageX < $(window).width()
      ) {
        $(".sidebar").css("flex-basis", x);
        Scrollbar.destroy(elem);
        Scrollbar.init(elem, scrollbarOptions);
      } else if (x < minSidebarWidth) {
        $(".sidebar").css("flex-basis", 0);
      }
    });
  });
  $(document).mouseup(function(e) {
    $(document).unbind("mousemove");
  });
}

function datatableInit() {
  var table = $("#datatable-main").DataTable({
    paging: false,
    ordering: false,
    info: false,
    retrieve: true
  });
  $("#datatablesearch").keyup(function() {
    table.search($(this).val()).draw();
  });
  var tdEvent;
  if (isMobileOrTablet()) {
    tdEvent = "click";
  } else {
    tdEvent = "dblclick";
  }
  $("#datatable-main td").on(tdEvent, function() {
    $td = $(this);
    $input = $("<input />")
      .addClass("form-control")
      .val($td.text());
    $td.html($input);
    $input.focus();
    $($input).on({
      keypress: function(e) {
        var key = e.which;
        if (key == 13) {
          tdInputEvent(this);
        }
      },
      blur: function() {
        tdInputEvent(this);
      }
    });
  });
}
function tdInputEvent(elem) {
  var val = $(elem).val();
  var index = $td.index();
  if (validationObj[index].regex.test(val)) {
    $td.removeClass('invalid');
  } else {
    $td.addClass('invalid');
  }
  if($('tr td.invalid:nth-child('+(index+1)+')').length){
    $('.first-header th:nth-child('+(index+1)+') > div:nth-child(4)').addClass('invalid')
  }
  else{
    $('.first-header th:nth-child('+(index+1)+') > div:nth-child(4)').removeClass('invalid')
  }
  $td.text(val);
  $(elem).remove();
}
function isMobileOrTablet() {
  return matchMedia("screen and (max-width: 800px)").matches;
}
function isTablet() {
  return matchMedia("(max-width:800px) and (min-width:700px)").matches;
}

function sideBarToggle() {
  $(".sidebar-resizer").on("click", function() {
    if (!$("body").hasClass("sidebar-open")) {
      $("body").addClass("sidebar-open");
    } else {
      $("body").removeClass("sidebar-open");
    }
  });
  $(".backdrop").on("click", function() {
    $("body").removeClass("sidebar-open");
  });
}
